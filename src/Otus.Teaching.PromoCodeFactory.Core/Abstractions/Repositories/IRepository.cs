﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IRepository<T>
        where T: BaseEntity
    {
        Task<IEnumerable<T>> GetAllAsync();
        
        Task<T> GetByIdAsync(Guid id);

        Task<bool> IsExistsAsync(Guid id);

        Task<Guid> AddAsync(T entity);
        
        Task RemoveByIdAsync(Guid id);
        
        /// <summary>
        /// В данной реализации никто не запрещает изменять Id объекта, никаких правил нет, но всё же я не дам изменять Id, так как это не правильно.
        /// </summary>
        /// <param name="entity">Сущность с новыми параметрами</param>
        /// <returns></returns>
        Task UpdateAsync(T entity);
    }
}