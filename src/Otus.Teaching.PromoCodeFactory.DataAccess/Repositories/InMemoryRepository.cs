﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<bool> IsExistsAsync(Guid id) => Task.FromResult(Data.FirstOrDefault(x => x.Id == id) != null);

        public Task<Guid> AddAsync(T entity)
        {
            Guid newId;
            do
            {
                newId = Guid.NewGuid();
            }
            while (Data.Any(x => x.Id.Equals(newId)));

            entity.Id = newId;
            (Data as IList<T>).Add(entity);

            return Task.FromResult(newId);
        }

        public Task RemoveByIdAsync(Guid id)
        {
            var entity = Data.First(x => x.Id.Equals(id));
            (Data as IList<T>).Remove(entity);
            return Task.CompletedTask;
        }

        public Task UpdateAsync(T entity)
        {
            var entityToUpdate = Data.First(x => x.Id.Equals(entity.Id));
            var entityProperties = entity.GetType().GetProperties().Where(q => q.SetMethod != null && q.Name != nameof(BaseEntity.Id));
            foreach (var property in entityProperties)
            {
                var pType = property.PropertyType;
                if (pType.IsGenericType && pType.GetGenericTypeDefinition() == typeof(List<>))
                {
                    var list = property.GetValue(entity);
                    if (list != null) property.SetValue(entityToUpdate, list); //Чтобы стереть делаем clear.
                }
                else property.SetValue(entityToUpdate, property.GetValue(entity));
            }
            return Task.CompletedTask;
        }
    }
}